# CS310
Various data structures written in Java 8, using Eclipse, from the Fall 2017 semester.

For the below commands to work, run: `cd src`

## Program 1
Driver

Compile: `javac P1Driver.java`

Execute: `java P1Driver`

Tester

Compile: `javac P1Tester.java`

Execute: `java P1Tester`

Grader

Compile: `javac Grader1.java`

Execute: `java Grader1`

## Program 2
Queue

Compile: `javac P2DriverQueue.java`

Execute: `java P2DriverQueue`

Stack

Compile: `javac P2TesterStack.java`

Execute: `java P2TesterStack`

Grader

Compile: `javac Grader2.java`

Execute: `java Grader2`

## Program 3
Maze Solver

Compile: `javac MazeSolver.java`

Execute: `java MazeSolver`

## Program 4
Missing a test/grade program.
